# Experis_Week2_Task12

## Task 12: 8-queens problem

#### Group members: Ørjan Storås, Kjetil Huy Tran

8-queens problem is a problem, where one try to place 8 queens onto a 8x8 chess board without having any of them threatening each other. 
This specific task want the 8-queens problem to be solved given a fixed custom position of the first queen. The position of the queen should be given using algebraic notation. Finally the board with the queens should be displayed.

In additional to solving this specific task, the program some additional features. Among them, the feature to change the size of the board and number of queens by N-queens on NxN board with N > 4.

