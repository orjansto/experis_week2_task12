﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12_8_Queen_problem
{
    class Example
    {
        private int boardSize;
        private string[] alphabet;

        public Example(int size)
        {
            boardSize = size;
            alphabet = new[] { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P" };
        }

        private void PrintSolution(int[,] board)
        {
            bool color = true;
            bool colorPrev = true;
            for (int i = 0; i < boardSize; ++i)
            {
                for (int j = 0; j < boardSize; ++j)
                {
                    
                    if (color)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                        if(board[i, j]==1)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write(" Q");
                        }
                        else
                        {
                            Console.Write("  ");
                        }
                        
                        Console.ResetColor();
                        color = false;
                    }
                    else
                    {
                        if (board[i, j] == 1)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.Write(" Q");
                        }
                        else
                        {
                            Console.Write("  ");
                        }
                        color = true;
                    }
                }
                Console.ResetColor();
                Console.WriteLine($"| {boardSize-i}");
                colorPrev = (!colorPrev);
                color = colorPrev;
            }
            for (int i = 0; i < boardSize; i++)
            {
                Console.Write(" {0}", alphabet[i]);
            }
            Console.WriteLine();
        }

        private bool IsSafe(int[,] board, int row, int col)
        {
            int i, j;

            //Checks if current row is safe
            for (i = 0; i < boardSize; ++i)
                if (Convert.ToBoolean(board[row, i]))
                    return false;

            //Back up diagonal check
            for (i = row, j = col; i >= 0 && j >= 0; --i, --j)
                if (Convert.ToBoolean(board[i, j]))
                    return false;

            //Back down diag check
            for (i = row, j = col; j >= 0 && i < boardSize; ++i, --j)
                if (Convert.ToBoolean(board[i, j]))
                    return false;

            //Front up diagonal check
            for (i = row, j = col; i >= 0 && j < boardSize; --i, ++j)
                if (Convert.ToBoolean(board[i, j]))
                    return false;

            //Front down diag check
            for (i = row, j = col; j < boardSize && i < boardSize; ++i, ++j)
                if (Convert.ToBoolean(board[i, j]))
                    return false;

            return true;
        }
        public Queen AlgNotToQueen()
        {
            int x = 0;
            int y = 0;
            string algNot;
            bool rerun;
            Console.WriteLine($"Please place a queen on the Board.");
           
            do
            {
                rerun = false;
                string numberString = "";
                Console.WriteLine($"Max position: {alphabet[boardSize-1].ToLower()}{boardSize}");
                try
                {
                    algNot = Console.ReadLine().Trim().ToLower();
                    
                    string letterString = algNot.Substring(0, 1);
                    if (boardSize < 10)
                    {
                        if (algNot.Length != 2)
                        {
                            throw new Exception($"Please enter position of form {alphabet[boardSize - 1].ToLower()}{boardSize}");
                        }
                        numberString = algNot.Substring(1, 1);
                    }
                    else
                    {//Make prog handle double digit reply
                        if (algNot.Length != 2 && algNot.Length != 3)
                        {
                            throw new Exception($"Please enter position of form {alphabet[boardSize - 1].ToLower()}{boardSize}");
                        }
                        if(algNot.Length == 2)
                        {
                            numberString = algNot.Substring(1, 1);
                        }
                        else
                        {
                            numberString = algNot.Substring(1, 2);
                        }
                        
                    }
                    
                    if (int.TryParse(numberString, out int numberInt) && numberInt <= boardSize)
                    {
                        x = boardSize - numberInt;
                    }
                    else
                    {
                        throw new System.Exception($"input must contain number of max value {boardSize}");
                    }
                    bool match = false; 
                    for (int i = 0; i < boardSize; i++)
                    {
                        if (alphabet[i].ToLower() == letterString)
                        {
                            y = i;
                            match = true;
                        }  
                    }
                    if (!match)
                    {
                        throw new System.Exception($"inputs letter must be {alphabet[boardSize-1]} or a letter earlier in the alphabet.");
                    }
                }
                catch (System.Exception exp)
                {
                    Console.WriteLine(exp.Message);
                    Console.WriteLine("Press enter to contine:");
                    rerun = true;
                    Console.ReadLine();
                }
                

            } while (rerun);

            return new Queen(x,y);
        } 

        private bool SolveNQNew(int[,] board, int col, Queen queen)
        {
            board[queen.X, queen.Y] = 1;
            if(col == queen.Y)
            {
                col += 1;
            }
            if (col >= boardSize)
            {
                return true;
            }

            for (int i = 0; i < boardSize; ++i)
            {
                if (IsSafe(board, i, col))
                {
                    board[i, col] = 1;

                    if (SolveNQNew(board, col + 1, queen))
                    {
                        return true;
                    }
                    board[i, col] = 0;
                }
            }

            return false;
        }
        public int[,] CreateBoard(int size)
        {
            int[,] board = new int[size, size];
            for (int i = 0; i < size; i++)
            {
                for (int j = 0; j < size; j++)
                {
                    board[i, j] = 0;
                }
            }
            return board;
        }

        public bool Solve()
        {

            int[,] board = CreateBoard(boardSize);


            if (!SolveNQNew(board, 0, AlgNotToQueen()))
            {
                Console.WriteLine("No legal solutions");
                return false;
            }
            
            PrintSolution(board);
            return true;
        }
    }
}
