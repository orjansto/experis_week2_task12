﻿using System;

namespace Task12_8_Queen_problem
{
    class Program
    {
        static void Main(string[] args)
        {
            string choice = "FOOBAR";
            int minSize = 4;
            int maxSize = 15;

            Console.WriteLine("-----Task12-----");
            Console.WriteLine(" 8-Queen problem\n");


            int boardSize = 8;
            Example example = new Example(boardSize);
            example.Solve();



            //Code for rerunning the program
            do
            {
                Console.WriteLine("DO you want to rerun program with custom board sizes?");
                do
                {
                    Console.WriteLine("Please enter: y/n");
                    choice = Console.ReadLine().Trim().ToLower();
                } while (choice != "y" && choice != "n");
                if (choice == "y")
                {
                    bool success = false;
                    do
                    {
                        Console.WriteLine($"Please enter a boardSize N, of size {minSize}...{maxSize}:");
                        success = int.TryParse(Console.ReadLine(), out boardSize);
                    } while (!success||boardSize<minSize||boardSize>maxSize);
                    Example newBoard = new Example(boardSize);
                    newBoard.Solve();
                }
            } while (choice == "y");
            

        }
    }
}
