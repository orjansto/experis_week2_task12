﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12_8_Queen_problem
{
    class Queen
    {
        private int y;
        private int x;
        private char val;
        public Queen(int x, int y)
        {
            this.x = x;
            this.y = y;
            val = 'q';
        }

        public Queen(int x, int y, char val)
        {
            this.x = x;
            this.y = y;
            this.val = val;
        }

        public int Y { get => y;  }
        public int X { get => x;  }
        public char Val { get => val; }
    }
}
